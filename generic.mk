BINNAME?=$(shell basename $(CURDIR))

SRCS?=$(BINNAME).c
OBJS=$(SRCS:.c=.o)

VPATH=src

DEPS=$(addprefix $(VPATH)/,$(SRCS:.c=.d))

PREFIX?=/usr/local

CFLAGS+=-std=gnu99 -Wall -Wextra

DESTDIR?=
BINDIR?=$(PREFIX)/bin

ifeq ($(DEBUG),1)
CFLAGS+=-Og -ggdb
endif
export DEBUG

all: $(DEPS)
-include $(DEPS)

all: $(BINNAME)

%.d: %.c
	$(CC) $(CFLAGS) $< -MM -MT $(@:.d=.o) > $@

install: all
	mkdir -p "$(DESTDIR)$(BINDIR)"
	cp -fp "$(BINNAME)" "$(DESTDIR)$(BINDIR)"

uninstall:
	rm -f "$(DESTDIR)$(BINDIR)/$(BINNAME)"

clean:
	rm -f "$(BINNAME)"
	rm -f $(addprefix $(VPATH)/,$(OBJS)) $(DEPS)

.PHONY: all install uninstall clean
