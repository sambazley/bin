/* Copyright (C) 2019 Sam Bazley
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/select.h>
#include <time.h>

#define TIMEDIFF(a, b) (long) (((b.tv_sec - a.tv_sec) * 1000) \
                              + (b.tv_nsec - a.tv_nsec) / 1000000)

int main(int argc, char *argv[]) {
    char *end;
    int target;
    char buf [BUFSIZ];
    struct timespec earlier;

    if (argc == 2) {
        target = strtol(argv[1], &end, 10);
    }

    if (argc == 1) {
        target = 1000;
    } else if (argc > 2 || end == 0 || *end != 0) {
        printf("Usage: %s [interval in milliseconds]\n", argv[0]);
        return 1;
    }

    earlier.tv_sec = 0;

    while (1) {
        char *c = fgets(buf, BUFSIZ, stdin);

        if (c == 0) {
            break;
        } else {
            struct timespec now;
            clock_gettime(CLOCK_MONOTONIC_RAW, &now);

            if (TIMEDIFF(earlier, now) >= target) {
                printf(".\n");
                fflush(stdout);
                earlier = now;
            }
        }
    }
    return 0;
}
