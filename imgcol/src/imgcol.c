/* Copyright (C) 2019 Sam Bazley
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

#include <colortool.h>
#include <math.h>
#include <png.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <jpeglib.h>

#define DIST_THRESHOLD 0.1f
#define MIN_V 0.4f
#define MAX_V 0.9f
#define MIN_S 0.3f

#define MIN(a,b) (a>b?b:a)

struct Color {
    struct RGB rgb;
    struct HSV hsv;
};

struct ColorGroup {
    struct Color color;
    int weight;
};

static void parse_png(FILE *fp, struct Color **colors, unsigned int *count) {
    *colors = 0;

    png_structp png = png_create_read_struct(PNG_LIBPNG_VER_STRING, 0, 0, 0);
    if (!png) {
        return;
    }

    png_infop info = png_create_info_struct(png);
    if (!info) {
        png_destroy_read_struct(&png, 0, 0);
        return;
    }

    if (setjmp(png_jmpbuf(png))) {
        png_destroy_read_struct(&png, &info, 0);
        return;
    }

    png_init_io(png, fp);
    png_set_sig_bytes(png, 8);
    png_read_info(png, info);

    unsigned int width, height;
    int bit_depth, color_type;
    png_get_IHDR(png, info, &width, &height, &bit_depth, &color_type, 0, 0, 0);

    if (color_type == PNG_COLOR_TYPE_PALETTE) {
        png_set_expand(png);
    }
    if (color_type == PNG_COLOR_TYPE_GRAY && bit_depth < 8) {
        png_set_expand(png);
    }
    if (png_get_valid(png, info, PNG_INFO_tRNS)) {
        png_set_expand(png);
    }

    if (bit_depth == 16) {
        png_set_strip_16(png);
    }
    if (color_type == PNG_COLOR_TYPE_GRAY || color_type == PNG_COLOR_TYPE_GRAY_ALPHA) {
        png_set_gray_to_rgb(png);
    }

    int interlace_passes = png_set_interlace_handling(png);

    png_read_update_info(png, info);

    int rowbytes = png_get_rowbytes(png, info);
    int channels = (int) png_get_channels(png, info);
    unsigned char *image_data = malloc(rowbytes * height);
    png_bytep *row_pointers = malloc(sizeof(png_bytep) * height);

    for (unsigned int y = 0; y < height; y++) {
        row_pointers[y] = image_data + y * rowbytes;
    }

    for (int p = 0; p < interlace_passes; p++) {
        for (unsigned int y = 0; y < height; y++) {
            png_read_rows(png, &row_pointers[y], 0, 1);
        }
    }

    png_read_end(png, info);

    free(row_pointers);

    *count = width * height;
    *colors = malloc(sizeof(struct Color) * *count);

    for (unsigned int i = 0; i < *count; i++) {
        unsigned char *p = image_data + i * channels;
        (*colors)[i].rgb.r = p[0] / 255.f;
        (*colors)[i].rgb.g = p[1] / 255.f;
        (*colors)[i].rgb.b = p[2] / 255.f;
        (*colors)[i].hsv = rgb2hsv((*colors)[i].rgb);
    }

    free(image_data);

    png_destroy_read_struct(&png, &info, 0);
}

static void parse_jpeg(FILE *fp, struct Color **colors, unsigned int *count) {
    struct jpeg_decompress_struct jpeg;
    struct jpeg_error_mgr err_pub;
    jmp_buf jmpbuf;

    *colors = 0;

    memset(&jpeg, 0, sizeof(struct jpeg_decompress_struct));

    jpeg.err = jpeg_std_error(&err_pub);
    if (setjmp(jmpbuf)) {
        jpeg_destroy_decompress(&jpeg);
        return;
    }

    jpeg_create_decompress(&jpeg);
    jpeg_stdio_src(&jpeg, fp);
    jpeg_read_header(&jpeg, TRUE);

    jpeg_start_decompress(&jpeg);

    int row_stride = jpeg.output_width * jpeg.output_components;
    JSAMPARRAY buffer = (*jpeg.mem->alloc_sarray)((j_common_ptr) &jpeg, JPOOL_IMAGE, row_stride, 1);

    *count = jpeg.output_width * jpeg.output_height;
    *colors = malloc(sizeof(struct Color) * *count);

    while (jpeg.output_scanline < jpeg.output_height) {
        jpeg_read_scanlines(&jpeg, buffer, 1);

        for (unsigned int i = 0; i < jpeg.output_width; i++) {
            int ci = jpeg.output_width * (jpeg.output_scanline - 1) + i;
            int bi = i * jpeg.output_components;

            (*colors)[ci].rgb.r = buffer[0][bi + 0] / 255.f;
            (*colors)[ci].rgb.g = buffer[0][bi + 1] / 255.f;
            (*colors)[ci].rgb.b = buffer[0][bi + 2] / 255.f;
            (*colors)[ci].hsv = rgb2hsv((*colors)[ci].rgb);
        }
    }

    jpeg_finish_decompress(&jpeg);
    jpeg_destroy_decompress(&jpeg);
}

static int parse_file(char *path, struct Color **colors, unsigned int *count) {
    FILE *fp = fopen(path, "rb");
    if (!fp) {
        fprintf(stderr, "Failed to open file\n");
        return 1;
    }

#define sig_size 8
    unsigned char sig [sig_size];
    if (fread(sig, 1, sig_size, fp) != sig_size) {
        fprintf(stderr, "Failed to read signature from file\n");
        return 1;
    }
    unsigned char jpeg_sig [3] = {0xff, 0xd8, 0xff};

    int ret;
    if (!png_sig_cmp(sig, 0, sig_size)) {
        parse_png(fp, colors, count);
        ret = !*colors;
    } else if (!memcmp(sig, jpeg_sig, 3)) {
        rewind(fp);

        parse_jpeg(fp, colors, count);
        ret = !*colors;
    } else {
        fprintf(stderr, "Invalid file\n");
        ret = 1;
    }

    fclose(fp);
    return ret;
}

static struct ColorGroup *find_group(struct ColorGroup *g, int gc, struct Color c) {
    struct ColorGroup *closest = 0;
    float min_dist;

    for (int i = 0; i < gc; i++) {
        struct ColorGroup *group = &g[i];
        float dist = fabs(c.hsv.h - group->color.hsv.h);

        if (!closest || dist < min_dist) {
            closest = group;
            min_dist = dist;
        }
    }

    if (closest && min_dist < DIST_THRESHOLD) {
        return closest;
    } else {
        return 0;
    }
}

static void group_colors(struct Color *colors, unsigned int count, struct ColorGroup **g, int *gc) {
    struct ColorGroup *groups = 0;
    int group_count = 0;

    for (unsigned int i = 0; i < count; i++) {
        struct Color c = colors[i];
        struct ColorGroup *group = find_group(groups, group_count, c);

        if (c.hsv.v < MIN_V || c.hsv.v > MAX_V || c.hsv.s < MIN_S) {
            continue;
        }

        if (!group) {
            groups = realloc(groups, sizeof(struct ColorGroup) * ++group_count);
            group = &groups[group_count - 1];
            group->weight = 0;
            memset(&(group->color.hsv), 0, sizeof(struct HSV));
        }

        group->color.hsv.h = group->color.hsv.h * group->weight + c.hsv.h;
        group->color.hsv.s = group->color.hsv.s * group->weight + c.hsv.s;
        group->color.hsv.v = group->color.hsv.v * group->weight + c.hsv.v;

        group->weight++;

        group->color.hsv.h /= group->weight;
        group->color.hsv.s /= group->weight;
        group->color.hsv.v /= group->weight;
    }

    for (int i = 0; i < group_count; i++) {
        struct ColorGroup *g = &groups[i];
        g->color.rgb = hsv2rgb(g->color.hsv);
    }

    *g = groups;
    *gc = group_count;
}

static int generate_palette(struct ColorGroup *groups, int group_count) {
    struct Color *background = 0;
    int max_weight = 0;

    for (int i = 0; i < group_count; i++) {
        struct ColorGroup *g = &groups[i];
        if (g->weight > max_weight && g->color.hsv.v < 0.5f && g->color.hsv.s > 0.5f) {
            max_weight = g->weight;
            background = &(g->color);
        }
    }

    if (!background) {
        fprintf(stderr, "Couldn't find a background color\n");
        return 1;
    }

    int color_order [] = {0, 2, 1, 4, 5, 3};
    struct Color *colors [6] = {0};

    for (int i = 0; i < 6; i++) {
        int target_h = 0;
        float max_h_dist = 0;

        struct Color *closest;

        for (int j = 0; j < 6; j++) {
            if (colors[j]) {
                continue;
            }

            float min_h_dist = 6;
            struct Color *closest_buffer = 0;

            for (int k = 0; k < group_count; k++) {
                float h = groups[k].color.hsv.h;
                float h_dist = MIN(fabs(h - j), fabs(j - h + 6));

                int used = 0;
                for (int l = 0; l < 6; l++) {
                    if (colors[l] == &groups[k].color) {
                        used = 1;
                        break;
                    }
                }

                if ((!closest_buffer || h_dist < min_h_dist) && !used) {
                    closest_buffer = &groups[k].color;
                    min_h_dist = h_dist;
                }
            }

            if (min_h_dist >= max_h_dist) {
                max_h_dist = min_h_dist;
                target_h = j;
                closest = closest_buffer;
            }
        }

        colors[target_h] = closest;
    }

    printf("#%02x%02x%02x\n",
            (int) (background->rgb.r * 255.f),
            (int) (background->rgb.g * 255.f),
            (int) (background->rgb.b * 255.f));

    for (int i = 0; i < 6; i++) {
        struct Color *color = colors[color_order[i]];
        printf("#%02x%02x%02x\n",
                (int) (color->rgb.r * 255.f),
                (int) (color->rgb.g * 255.f),
                (int) (color->rgb.b * 255.f));
    }

    return 0;
}

int main(int argc, char *argv[]) {
    if (argc != 2) {
        fprintf(stderr, "Usage: %s <image>\n", argv[0]);
        return 1;
    }

    struct Color *colors;
    unsigned int color_count;

    if (parse_file(argv[1], &colors, &color_count)) {
        return 1;
    }

    struct ColorGroup *groups;
    int group_count;

    group_colors(colors, color_count, &groups, &group_count);

    free(colors);

    if (generate_palette(groups, group_count)) {
        free(groups);
        return 1;
    }

    free(groups);
    return 0;
}
