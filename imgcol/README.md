# imgcol
A simple color scheme generator.

An example configuration can be seen [here](https://gitlab.com/sambazley/dotfiles/tree/master/filesdir/home/user/.imgcol).
